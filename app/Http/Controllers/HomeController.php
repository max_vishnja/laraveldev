<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\NewsModalRequest;
use App\News;
use Illuminate\Http\Request;
use App\Category;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{
    /**
     * View all news to main page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cats = Category::latest('created_at')->get();
        $news = News::latest('published_at')->published()->get();
        return view('home', compact('cats', 'news'));
    }

    /**
     * Show one news
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $alias
     */
    public function show(News $news)
    {
        return view('show_news', compact('news'));

    }

    /**
     * Add news with image
     * @param $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . $file->getClientOriginalName();
            $file->move('userfiles/', $name);
            $tn_image = Image::make(public_path('userfiles/') . $path)->fit(300)->save(public_path() . '/userfiles/thumb/' . $name);
            $all = $request->all();
            $all['image'] = $name;
            $all['tn_image_path'] = 'userfiles/thumb/' . $name;
            $all['image_path'] = $path;
        } else {
            $all = $request->all();
        }
        News::create($all);
        flash()->overlay("Your news has been added","Congratulations");
        return redirect()->back();

    }


}
