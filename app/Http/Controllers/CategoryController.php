<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Auth
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * View all categories
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cats = Category::latest('created_at')->get();
        return view('category.index', compact('cats'));

    }

    /**
     * Create view add category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Add category
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CategoryRequest $request)
    {
        Category::create($request->all());
        return redirect()->route('admin.categories.index');

    }

    /**
     * Edit category
     * @param Category $cats
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $id
     */
    public function edit(Category $cats)
    {
        return view('category.edit', compact('cats'));

    }

    /**
     * Update category
     * @param Category $cats
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param $id
     */
    public function update(Category $cats, CategoryRequest $request)
    {
        $cats->update($request->all());
        return redirect()->route('admin.categories.index');

    }

    /**
     * Delete category
     * @param Category $cats
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     * @internal param $id
     */
    public function destroy(Category $cats)
    {
        $cats->delete();
        return redirect()->route('admin.categories.index');
    }

}
