<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Requests\NewsModalRequest;
use Illuminate\Http\Request;

use App\News;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    /**
     * Auth
     * NewsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * View list news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = News::latest('published_at')->get();
        return view('news.index', compact('news'));

    }

    /**
     * Show one news
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $id
     */
    public function show(News $news)
    {
        return view('news.show', compact('news'));

    }

    /**
     * Create view add news with category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::lists('title', 'id');
        return view('news.create', compact('categories'));

    }

    /**
     * Add news with category and image
     * @param NewsModalRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(NewsModalRequest $request)
    {
        $file = $request->file('image');
        $name = time() . $file->getClientOriginalName();
        $file->move('userfiles/', $name);
        $tn_image = Image::make(public_path() . '/userfiles/' . $name)->fit(300)->save(public_path() . '/userfiles/thumb/' . $name);
        $all = $request->all();
        $all['image'] = $name;
        $all['tn_image_path'] = 'userfiles/thumb/' . $name;
        $all['image_path'] = '/userfiles/' . $name;
        News::create($all);
        return redirect()->route('admin.news.index');

    }

    /**
     * Edit news
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $id
     */
    public function edit(News $news)
    {
        $categories = Category::lists('title', 'id');
        return view('news.edit', compact('news', 'categories'));

    }

    /**
     * Update news
     * @param News $news
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param $id
     */
    public function update(News $news, ArticleRequest $request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $name = time() . $file->getClientOriginalName();
            $file->move('userfiles/', $name);
            $tn_image = Image::make(public_path() . '/userfiles/' . $name)->fit(300)->save(public_path() . '/userfiles/thumb/' . $name);
            $all = $request->all();
            $all['image'] = $name;
            $all['tn_image_path'] = 'userfiles/thumb/' . $name;
            $all['image_path'] = '/userfiles/' . $name;
            $news->update($all);
        } else {

            $news->update($request->all());
        }
        return redirect()->route('admin.news.index');

    }

    /**
     * Delete news
     * @param News $news
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     * @internal param $id
     */
    public function destroy(News $news)
    {

        $news->delete();

        return redirect()->route('admin.news.index');
    }


}
