<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'NewsController@index');
        Route::resource('news', 'NewsController');
        Route::resource('categories', 'CategoryController');
    });
    Route::get('welcome/{locale}', function ($locale) {
        App::setLocale($locale);

        //
    });
    Route::get('/', 'HomeController@index');
    Route::resource('news', 'HomeController', ['except' => ['update', 'destroy']]);

});
//
//App::missing(function($exception)
//{
//    return Response::view('error', array(), 404);
//});

