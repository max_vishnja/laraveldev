<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";
    protected $fillable = ['title', 'content', 'image', 'alias', 'meta_description', 'meta_keywords', 'image_path', 'tn_image_path', 'category_id', 'publish', 'published_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function scopePublished($query)
    {

        $query->where('publish', '=', '1')->limit('8');

    }
}
