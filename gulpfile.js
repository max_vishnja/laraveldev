var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
            '/bootstrap/assets/js/vendor/jquery.min.js',
            '/bootstrap/dist/js/bootstrap.min.js',
            '/bootstrap/assets/js/vendor/holder.min.js',
            '/bootstrap/assets/js/ie8-responsive-file-warning.js',
            '/bootstrap/assets/js/ie-emulation-modes-warning.js',
            '/bootstrap/assets/js/ie10-viewport-bug-workaround.js'],
        'public/js/dependencies.js', 'node_modules')

        .scripts([
            '/jquery.isotope.min.js',
            '/scripts.js'],
            'public/js/app.js')

        .styles([
            "carousel.css"
        ], 'public/css/style-front.css')

        .styles([
            "dashboard.css"
        ], 'public/css/style-admin.css')

        .styles([
            "/bootstrap/dist/css/bootstrap.min.css",
            "/bootstrap/assets/css/ie10-viewport-bug-workaround.css"
        ], 'public/css/dependencies.css', 'node_modules');


});