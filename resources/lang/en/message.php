<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Views Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'more' => 'More',
    'showcat' => 'Show all',
    'addnews' => 'Add your news',
    'choosecat' => 'Please choose category:',
    'header' => 'Latest news',
    'home' => 'Home',
    'en-site' => 'Enter to site',
    'en-admin' => 'Enter to admin',
    'sorry' => 'Sorry but no added news!!!
                Click Add your news',

];