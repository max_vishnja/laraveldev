@extends('app')
@section('content')

    <h1 class="page-header">{{$news->title}}</h1>

    <div class="container-fluid no-gutter">
        <div class="text-left">{{ $news->published_at }}</div>
        <div class="row">
            <img class="img-thumbnail my-image" src="{{$news->image_path}}">{!!$news->content!!}
        </div>
    </div>



@endsection
