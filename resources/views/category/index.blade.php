@extends('layouts.app')
@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="container-fluid">
            <h1 class="title">Category</h1>
            <hr>
            @foreach($cats as $cat)
                <div class="row">
                    <div class="col-md-8 col-xs-6">
                        <h4 class="col-md-1">
                            {{ $cat->title }}
                        </h4>
                    </div>
                    <div class="col-md-4 col-xs-4">

                        <div class="row">
                            <div class="col-md-10 col-xs-6 text-right">
                                <a class="btn-default btn btn-xs"
                                   href="{{action('CategoryController@edit',['categories'=>$cat->id])}}">
                                    <span class="glyphicon glyphicon-pencil btn" aria-hidden="true"></span>
                                </a>
                            </div>
                            @if(count($cats)>1)
                            <div class="col-md-2">
                                {!! Form::open(['method'=>'DELETE', 'action' => ['CategoryController@destroy', $cat->id],"class" =>'newsformс']) !!}
                                <button id="my-alert" type="button" class="btn btn-xs btn-default my-right">
                                    <span class="glyphicon glyphicon-remove btn" aria-hidden="true"></span>
                                </button>
                                {!! Form::close() !!}
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-offset-5 col-xs-6 col-xs-offset-3">
                {!! Form::open(['method'=>'GET','action' => ['CategoryController@create']]) !!}
                {!! Form::submit('Add category',['class'=>'btn btn-primary form-control']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

