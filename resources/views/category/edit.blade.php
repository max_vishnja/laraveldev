@extends('layouts.app')
@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1>Edit: {{ $cats->title }} </h1>
        @if($errors->any())
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

        @endif
        <hr>

        {!! Form::model($cats,['method'=>'PATCH', 'action' => ['CategoryController@update', $cats->id]]) !!}
        <div class="form-group">
            {!! Form::label('title','Title:') !!}
            {!! Form::text('title',null,['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('updated_at','Publish on:') !!}
            {!! Form::input('date','updated_at',date('Y-m-d'),['class'=>'form-control']) !!}
        </div>

        <div class="col-lg-2 col-md-offset-5">
            <div class="form-group">
                {!! Form::submit('Edit category',['class'=>'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop