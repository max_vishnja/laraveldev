<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description"
      content="{{ isset($news->meta_description) ? $news->meta_description : 'Welcome to Newsblog' }}">
<meta name="keywords" content="{{ isset($news->meta_kywords) ? $news->meta_kywords : 'Welcome to Newsblog' }}">
<link rel="icon" href="favicon.ico">

<title>{{ isset($news->title) ? $news->title : 'Welcome to Newsblog' }}</title>

<link href="/public/css/style-front.css" rel="stylesheet">
<link href="/public/css/dependencies.css" rel="stylesheet">
