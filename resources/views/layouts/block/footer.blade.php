<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="{!! asset('/js/dependencies.js') !!}"></script>
<script src="{!! asset('/js/ckeditor/ckeditor.js') !!}"></script>
<script src="{!! asset('/js/app.js') !!}"></script>
