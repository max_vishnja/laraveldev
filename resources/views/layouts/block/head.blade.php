<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<link rel="icon" href="favicon.ico">

<title>News dashboard</title>
<link href="/public/css/dependencies.css" rel="stylesheet">
<link href="/public/css/style-admin.css" rel="stylesheet">

