<div class="form-group">
    {!! Form::label('title','Title:') !!}
    {!! Form::text('title',null,['class'=>'form-control',"required"=>"required"]) !!}
</div>
<div class="form-group">
    {!! Form::label('content','Body:') !!}
    {!! Form::textarea('content',null,['class'=>'form-control','id'=>"editor1","required"=>"required"]) !!}
</div>
<div class="form-group">
    {!! Form::label('alias','Alias:') !!}
    {!! Form::text('alias',null,['class'=>'form-control',"required"=>"required"]) !!}
</div>
<div class="form-group">
    {!! Form::label('category_id','Category:') !!}
    {!! Form::select('category_id',\App\Category::all()->lists('title','id'),['class'=>'form-control']) !!}
</div>
<div class="form-group ">
    {!! Form::label('image','Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="form-group">
    <input type="hidden" name="publish" value="0">
    <input type="hidden" name="published_at" value="{{date('Y-m-d')}}">
</div>
