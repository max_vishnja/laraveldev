<!DOCTYPE html>
<html>
<head>
    @include('layouts.block.head')

</head>
<body>

<div class="container">

    @yield('content')

</div>
@include('layouts.block.footer')
</body>
</html>
