@extends('app')
@section('content')

    <h1 class="page-header">{{ trans('message.header') }}</h1>
    <div class="container-fluid">
        @if(count($news))
            <div class="row">
                <div class="col-md-4">
                    <h3>{{ trans('message.choosecat') }}</h3>
                </div>
                <div class="col-md-8 text-left">
                    <div id="filters" class="button-group">
                        <button class="btn btn-primary btn-lg" data-filter="*">{{ trans('message.showcat') }}</button>
                        @foreach($cats as $cat)
                            <button class="btn btn-primary btn-lg" data-filter=".{{$cat->id}}">{{$cat->title}}</button>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="container-fluid no-gutter">

                <div id="posts" class="row">
                    @foreach($news as $new)

                        <div id="1" class="item {{$new->category_id}} col-sm-3">

                            <div class="item-wrap">
                                <h4>{!! link_to_route('news.show', $new->title, [$new->id]) !!}</h4>
                                <img class="img-responsive img-thumbnail" src="{{$new->tn_image_path}}">
                                <div class="text-right">{{$new->published_at }}</div>
                                <p class="prew">{!!  substr(strip_tags($new->content), 0, 100)  !!}</p>
                                <div class="row text-center">
                                    {!! link_to_route('news.show', trans('message.more'), [$new->id],['class'=>'btn btn-primary']) !!}
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <h2>{{ trans('message.sorry') }}</h2>
        @endif
    </div>
@endsection
