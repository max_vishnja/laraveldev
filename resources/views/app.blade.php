<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.block.head_site')
</head>

<body>
<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">My Newsblog</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-left">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}">{{ trans('message.home') }}</a></li>
                        <li><a style="color:white; cursor:pointer;" data-toggle="modal"
                               data-target="#myModal">{{ trans('message.addnews') }}</a></li>
                    </ul>
                </div>
                <div id="navbar" class="navbar-collapse collapse navbar-right">
                    <ul class="nav navbar-nav">
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>

                        @else
                            <li><a href="{{ url('/admin') }}">{{ trans('message.en-admin') }}</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                    </li>
                                </ul>
                            </li>

                        @endif
                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>
<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        @include('flash::message')
        @yield('content')
    </div><!-- /.row -->

    <!-- FOOTER -->
    <footer>
    </footer>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('message.addnews') }}</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['action' => ['HomeController@store'],'files' => true, "id"=>"mod-form"]) !!}
                    @include('layouts.simple_form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="save" class="btn btn-primary">Add</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div><!-- /.container -->
@include('layouts.block.footer')
</body>
</html>
