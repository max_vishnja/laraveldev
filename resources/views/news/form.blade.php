<div class="form-group">
    {!! Form::label('title','Title:') !!}
    {!! Form::text('title',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('content','Body:') !!}
    {!! Form::textarea('content',null,['class'=>'form-control','id'=>"editor1"]) !!}
</div>
<div class="form-group">
    {!! Form::label('alias','Alias:') !!}
    {!! Form::text('alias',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('category_id','Category:') !!}
    {!! Form::select('category_id', $categories) !!}
</div>
<div class="form-group">
    {!! Form::label('meta_description','Meta description:') !!}
    {!! Form::text('meta_description',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('meta_keywords','Meta keywords:') !!}
    {!! Form::text('meta_keywords',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('image','Image:') !!}
    {!! Form::file('image') !!}
    @if(!empty($news->image) and isset($news->image))
        <img data-src="holder.js/100%x180" width="200" src="{{$news->image_path}}">
    @endif
</div>
<div class="form-group">
    {!! Form::label('published_at','Publish on:') !!}
    {!! Form::input('date','published_at',date('Y-m-d'),['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('publish','Publish:') !!}
    {!! Form::select('publish',array('0'=>'No','1'=>'Yes')) !!}
</div>
