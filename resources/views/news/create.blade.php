@extends('layouts.app')
@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="container-fluid">
        <h1>Create News</h1>
            @if($errors->any())
                <ul class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

            @endif
        <hr>

        {!! Form::open(['url' => '/admin/news','files' => true]) !!}
        @include('news.form')
            <div class="row">
                <div class="col-lg-2 col-md-offset-5">
                    <div class="form-group">
                        {!! Form::submit('Add news',['class'=>'btn btn-primary form-control']) !!}
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>

@stop