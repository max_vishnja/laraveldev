@extends('layouts.app')
@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="container-fluid">
            <h1 class="title">News</h1>
            <hr>
            @foreach($news as $new)
                <div class="row">
                    <div class="col-md-8 col-xs-6">
                        <h4 class="col-md-3">
                            <a href="{{url('/admin/news',$new->id) }}">{{ $new->title }}</a>
                        </h4>
                    </div>
                    <div class="col-md-4 col-xs-4">

                        <div class="row">
                            <div class="col-md-10 col-xs-6 text-right">
                                <a class="btn-default btn btn-xs" href="{{action('NewsController@edit',['news'=>$new->id])}}">
                                    <span class="glyphicon glyphicon-pencil btn" aria-hidden="true"></span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                {!! Form::open(['method'=>'DELETE', 'action' => ['NewsController@destroy', $new->id],"class"=>'newsformn']) !!}
                                <button id="my-alert" type="button" class="btn btn-xs btn-default my-right">
                                    <span  class="glyphicon glyphicon-remove btn" aria-hidden="true"></span>
                                </button>
                                {!! Form::close() !!}
                            </div>


                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-offset-5 col-xs-6 col-xs-offset-3">
                {!! Form::open(['method'=>'GET','action' => ['NewsController@create']]) !!}
                {!! Form::submit('Add news',['class'=>'btn btn-primary form-control']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

