$( document ).ready(function() {
    /* activate jquery isotope */
    var $container = $('#posts').isotope({
        itemSelector : '.item',
        isFitWidth: true
    });

    $(window).smartresize(function(){
        $container.isotope({
            columnWidth: '.col-sm-3'
        });
    });

    $container.isotope({ filter: '*' });

    // filter items on button click
    $('#filters').on( 'click', 'button', function() {
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });
    });
    $('#flash-overlay-modal').modal();


    $('.newsformn').on('click', function() {
        if (confirm('Do you really want to delete the news?')){
            $(this).submit();
        }
    });

    $('.newsformс').on('click', function() {
        if (confirm('Are you sure? This will erase all related items')){
            $(this).submit();
        }
    });

    var editor = CKEDITOR.replace( 'editor1' );
});








